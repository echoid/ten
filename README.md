Ten (10)
===

The realisation of game "Упрямый осел" from the USSR on lua with [LÖVE framework](http://love2d.org/).

## On Linux
#### Preparing
Install packages: librsvg2-bin, [love](http://love2d.org/).
#### Building
`$ ./build.sh`
#### Running
`$ love bin/`

## On Windows
#### Preparing
Install [ImageMagick](http://www.imagemagick.org/script/binary-releases.php#windows), [love](http://love2d.org/) (add path to Path environment variable).
#### Building
`> build.bat`
#### Running
`> love bin/`