@echo off & setlocal enabledelayedexpansion

set src=res
set out=bin-res

set folder[1]=mdpi
set folder[2]=hdpi
set folder[3]=xhdpi
set folder[4]=xxhdpi
set folder[5]=xxxhdpi

set density[1]=90
set density[2]=135
set density[3]=180
set density[4]=270
set density[5]=360

rmdir /S /Q %out% 2> nul
mkdir %out%
xcopy /E %src%\root %out% > nul

for /l %%i in (1, 1, 5) do ( 
	echo   !folder[%%i]!
	mkdir %out%\!folder[%%i]!
	xcopy /E %src%\scalable %out%\!folder[%%i]! > nul
	for /f %%f in ('dir /b %out%\!folder[%%i]!\') do (
		convert -background none -density !density[%%i]! "%out%\!folder[%%i]!\%%f" "%out%\!folder[%%i]!\%%~nf.png" > nul
		del %out%\!folder[%%i]!\%%f 2> nul
	)
	xcopy /E %src%\non-scalable %out%\!folder[%%i]! > nul
)