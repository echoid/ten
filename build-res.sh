#!/bin/bash

src="res"
out="bin-res"

folder[1]="mdpi"
folder[2]="hdpi"
folder[3]="xhdpi"
folder[4]="xxhdpi"
folder[5]="xxxhdpi"

density[1]=90
density[2]=135
density[3]=180
density[4]=270
density[5]=360

rm -fdr $out
mkdir -p $out
cp $src/root/* $out/

for i in {1..5}; do
	printf "\n	${folder[$i]}"
	mkdir $out/${folder[$i]}
	cp $src/scalable/* $out/${folder[$i]}/
	for file in $out/${folder[$i]}/*; do
		rsvg-convert -d ${density[$i]} $file -o ${file%.svg}.png
		rm $file
	done
	cp $src/non-scalable/* $out/${folder[$i]}/
done