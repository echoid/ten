ScreenMenu = Screen:new()

function ScreenMenu:load()
	game:loadPalette()
	graphics.setBackgroundColor(game:getColorPalette("bg"))

	self.buttons = {}

	self.exitButton = SpriteButton:new("button-exit", "lt", game:scale(20), game:scale(20), event.quit)
	table.insert(self.buttons, self.exitButton)

	local cb = function() game:pushScreen(ScreenAbout) end
	self.aboutButton = SpriteButton:new("button-about", "rt", game.width - game:scale(20), game:scale(20), cb)
	table.insert(self.buttons, self.aboutButton)

	local y = game.height - game:scale(60) - game:scale(20)
	local cb = function() audio.setVolume(2 - self.volumeButton.value) end
	self.volumeButton = SpriteToogleButton:new({"button-volume", "button-volume-off"}, "lb", game:scale(20), y, cb)
	table.insert(self.buttons, self.volumeButton)

	local x = game.width - game:scale(20)
	local cb = function() self:onLanguageButtonClick() end
	self.languageButton = SpriteToogleButton:new({"button-lang-en", "button-lang-ru"}, "rb", x, y, cb)
	table.insert(self.buttons, self.languageButton)
	
	local y = game.height / 2 + game:scale(10) - game.resources["system-big"]:getHeight() / 2
	local cb = function() game:pushScreen(ScreenCampaign) end
	self.campaignButton = TextButton:new(game:translate("campaign"), game.width / 2, y, cb)
	table.insert(self.buttons, self.campaignButton)

	local y = game.height / 2 + game:scale(10) + game.resources["system-big"]:getHeight() / 2
	local cb = function() end
	self.quickButton = TextButton:new(game:translate("quick_game"), game.width / 2, y, cb)
	table.insert(self.buttons, self.quickButton)

	local y = game.height - game:scale(60) / 2 - self.exitButton.height / 2
	local cb = function() end
	self.prevButton = SpriteButton:new("button-left", "lt", game:scale(16), y, cb)
	table.insert(self.buttons, self.prevButton)

	local cb = function()  end
	self.nextButton = SpriteButton:new("button-right", "rt", game.width - game:scale(16), y, cb)
	table.insert(self.buttons, self.nextButton)

	self.title = {}
	self.title.sprite = game.resources["title"]
	self.title.oX = self.title.sprite:getWidth() / 2
	self.title.oY = self.title.sprite:getHeight() / 2
	self.title.x = game.width / 2
	self.title.y = game:scale(48) + self.title.oY
	self.title.angle = 0
	self.title.addAngle = 0

	self.skins = {}
	self.skins[1] = {}
	self.skins[1].sprite = game.resources["skin"]
	self.skins[1].x = game.width / 2 - self.skins[1].sprite:getWidth() / 2
	self.skins[1].y = game.height - game:scale(60) + (game:scale(60) / 2 - self.skins[1].sprite:getHeight() / 2)
end

function ScreenMenu:update()
	if input:isKeyPressed("escape") then game:exit() end

	for _, but in pairs(self.buttons) do
		but:update()
	end

	local tit = self.title

	if input:isMouseIn({tit.x - tit.oX, tit.y - tit.oY, tit.sprite:getWidth(), tit.sprite:getHeight()}) then
		if input:isMouseReleased() then
			tit.addAngle = tit.addAngle + math.pi / 3
		end
	end
	
	if tit.addAngle > 0 then
		local turnAngle = math.pi / 24

		if tit.addAngle < turnAngle then
			tit.angle = tit.angle + tit.addAngle
			tit.addAngle = 0
		else
			tit.angle = tit.angle + turnAngle
			tit.addAngle = tit.addAngle - turnAngle
		end
	end		
end

function ScreenMenu:onLanguageButtonClick()
	game:nextLang()
	self.campaignButton:setText(game:translate("campaign"))
	self.quickButton:setText(game:translate("quick_game"))
end

function ScreenMenu:draw()
	local tit = self.title
	graphics.draw(tit.sprite, tit.x, tit.y, tit.angle, _, _, tit.oX, tit.oY)

	graphics.setColor(game:getColorPalette("bg2"))
	local height = game:scale(60)
	graphics.rectangle("fill", 0, game.height - height, game.width, height)

	graphics.setColor(255, 255, 255)
	graphics.draw(self.skins[1].sprite, self.skins[1].x, self.skins[1].y)

	for _, but in pairs(self.buttons) do
		graphics.setColor(255, 255, 255)
		but:draw()
	end
end
