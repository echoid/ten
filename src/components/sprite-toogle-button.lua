SpriteToogleButton = {}

function SpriteToogleButton:new(sprites, align, x, y, callback)
	self.__index = self
	local loadedSprites = {}
	for i = 1, #sprites do
		loadedSprites[i] = game.resources[sprites[i]]
	end
	local button = SpriteButton:new(loadedSprites[1], align, x, y, callback)
	button.sprites = loadedSprites
	button.value = 1
	return setmetatable(button, self)
end

function SpriteToogleButton:update()
	self.isOver = input:isMouseIn({self.x - self.oX, self.y - self.oY, self.width, self.height})
	if input:isMouseReleased() and self.isOver then
		self.value = self.value + 1
		if self.value > #self.sprites then
			self.value = 1
		end
		self.sprite = self.sprites[self.value]
		self.isOver = false
		audio.play(self.sound)
		self.callback()
	end
	self.scale = (self.isOver and (input:isMouseDown() and 0.95 or 1.05) or 1)
end

function SpriteToogleButton:draw()
	graphics.draw(self.sprite, self.x, self.y, 0, self.scale, nil, self.oX, self.oY)
end