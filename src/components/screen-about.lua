ScreenAbout = Screen:new()

function ScreenAbout:load()
	self.timer = 0
	self.font = game.resources["system-small"]

	self.bg = {}
	self.bg.r, self.bg.g, self.bg.b = game:getColorPalette("bg")
	self.bg.opacity = 0
	self.bg.opacity2 = 1

	self.text = {}
	self.text.r, self.text.g, self.text.b = game:getColorPalette("text")

	self.labels = {}

	self.authors = {}
	self.authors.text = game:translate("authors")
	self.authors.x = game.width / 2 - self.font:getWidth(self.authors.text) / 2
	self.authors.y = game:scale(64)
	self.authors.offsetY = 0
	self.authors.maxOffsetY = 0
	self.authors.opacity = 0
	self.authors.timerOffset = 5
	table.insert(self.labels, self.authors)

	self.designer1 = {}
	self.designer1.text = game:translate("designer")
	self.designer1.x = game.width / 2 - self.font:getWidth(self.designer1.text) / 2
	self.designer1.y = game.height / 2 - self.font:getHeight() * 2 - game:scale(35)
	self.designer1.offsetY = 0
	self.designer1.maxOffsetY = game:scale(20)
	self.designer1.opacity = 0
	self.designer1.timerOffset = 7.5
	table.insert(self.labels, self.designer1)

	self.designer2 = {}
	self.designer2.text = "JohnScott"
	self.designer2.x = game.width / 2 - self.font:getWidth(self.designer2.text) / 2
	self.designer2.y = game.height / 2 - self.font:getHeight() - game:scale(35)
	self.designer2.offsetY = 0
	self.designer2.maxOffsetY = game:scale(20)
	self.designer2.opacity = 0
	self.designer2.timerOffset = 10
	table.insert(self.labels, self.designer2)

	self.developer1 = {}
	self.developer1.text = game:translate("developer")
	self.developer1.x = game.width / 2 - self.font:getWidth(self.developer1.text) / 2
	self.developer1.y = game.height / 2 + self.font:getHeight() - game:scale(55)
	self.developer1.offsetY = 0
	self.developer1.maxOffsetY = game:scale(20)
	self.developer1.opacity = 0
	self.developer1.timerOffset = 15
	table.insert(self.labels, self.developer1)

	self.developer2 = {}
	self.developer2.text = "e-andrew"
	self.developer2.x = game.width / 2 - self.font:getWidth(self.developer2.text) / 2
	self.developer2.y = game.height / 2 + self.font:getHeight() * 2 - game:scale(55)
	self.developer2.offsetY = 0
	self.developer2.maxOffsetY = game:scale(20)
	self.developer2.opacity = 0
	self.developer2.timerOffset = 17.5
	table.insert(self.labels, self.developer2)

	self.version = {}
	self.version.text = game.version
	self.version.x = game.width - self.font:getWidth(self.version.text) - game:scale(10)
	self.version.y = game.height - self.font:getHeight()
	self.version.offsetY = 0
	self.version.maxOffsetY = 0
	self.version.opacity = 0
	self.version.timerOffset = 5
	table.insert(self.labels, self.version)

	local x = game:scale(20)
	local y = game:scale(20)
	local cb = function() self:startTransition() end
	self.backButton = SpriteButton:new("button-back", "left", x, y, cb)
end

function ScreenAbout:startTransition()
	if self.timer == 45 then self.timer = 46 end
end

function ScreenAbout:update()
	local tim = self.timer

	self.bg.opacity = 17 * (tim < 15 and tim or 15)
	self.bg.opacity2 = 1 / (tim > 45 and 10 / (55 - tim) or 1)

	for _, label in pairs(self.labels) do
		local off = label.timerOffset
		local progress = (tim < off and 0 or (tim < 5 + off and (tim - off) / 5 or 1))
		label.opacity = progress * 255
		label.offsetY = progress * label.maxOffsetY
	end
	
	if input:isKeyPressed("escape") then self:startTransition() end

	self.backButton:update()

	if tim == 15 then self.timer, tim = 45, 45 end

	if (tim == 55) then
		game:goBack()
	end

	if (tim < 45) or (tim > 45 and tim < 55) then
		self.timer = self.timer + 1
	end
end

function ScreenAbout:draw()
	graphics.setFont(self.font)

	graphics.setColor(self.bg.r, self.bg.g, self.bg.b, self.bg.opacity * self.bg.opacity2)
	graphics.rectangle("fill", 0, 0, game.width, game.height)

	for _, label in pairs(self.labels) do
		graphics.setColor(self.text.r, self.text.g, self.text.b, label.opacity * self.bg.opacity2)
		graphics.print(label.text, label.x, (label.y + label.offsetY) * self.bg.opacity2)
	end

	graphics.setColor(255, 255, 255, self.bg.opacity * self.bg.opacity2)
	self.backButton:draw()
end