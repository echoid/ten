ScreenWin = Screen:new()

ScreenWin.BEST_SCORE = -1

function ScreenWin:load(params)
	local score = params.score

	if ScreenWin.BEST_SCORE == -1 then
		ScreenWin.BEST_SCORE = score
	end

	if ScreenWin.BEST_SCORE > score then
		ScreenWin.BEST_SCORE = score
	end

	self.timer = 0
	self.font = game.resources["system-small"]

	self.bg = {}
	self.bg.r, self.bg.g, self.bg.b = game:getColorPalette("bg")
	self.bg.opacity = 0
	self.bg.opacity2 = 1

	self.text = {}
	self.text.r, self.text.g, self.text.b = game:getColorPalette("text")

	self.award = {}
	self.award.sprite = game.resources["award"]
	self.award.width = self.award.sprite:getWidth()
	self.award.height = self.award.sprite:getHeight()
	self.award.oX = self.award.width / 2
	self.award.oY = self.award.height / 2
	self.award.x = game.width / 2
	self.award.y = game.height / 2
	self.award.scale = 0
	self.award.offsetY = 0
	self.award.angle = 0
	self.award.timer = 0

	self.complete = {}
	self.complete.text = game:translate("complete")
	self.complete.x = game.width / 2 - self.font:getWidth(self.complete.text) / 2
	self.complete.y = game.height / 2 - self.font:getHeight() / 2
	self.complete.opacity = 0

	self.yourscore = {}
	self.yourscore.text = game:translate("your_score") .. score
	self.yourscore.x = game.width / 2 - self.font:getWidth(self.yourscore.text) / 2
	self.yourscore.y = game.height / 2 - self.font:getHeight() / 2 + game:scale(30)
	self.yourscore.opacity = 0
	self.yourscore.offsetY = 0

	self.bestscore = {}
	self.bestscore.text = game:translate("best_score") .. ScreenWin.BEST_SCORE
	self.bestscore.x = game.width / 2 - self.font:getWidth(self.bestscore.text) / 2
	self.bestscore.y = game.height / 2 - self.font:getHeight() / 2 + game:scale(70)
	self.bestscore.opacity = 0
	self.bestscore.offsetY = 0

	self.backButton = SpriteButton:new("button-back", "left", game:scale(20), game:scale(20), function() self:startTransition(2) end)
	self.nextButton = SpriteButton:new("button-next", "right", game.width - game:scale(20), game:scale(20), function() self:startTransition(1) end)
end

function ScreenWin:startTransition(n)
	if self.timer == 45 then
		self.timer = 46
		self.backCount = n

		if n == 2 then game.screens:get(game.screens.length - 1).hide = true end
	end
end

function ScreenWin:update()
	if input:isMouseIn({self.award.x - self.award.oX, self.award.y - self.award.oY + self.award.offsetY, self.award.width, self.award.height}) then
		if input:isMouseReleased() and self.award.timer == 0 then
			self.award.timer = 15
		end
	end

	self.bg.opacity = 17 * (self.timer < 15 and self.timer or 15)
	self.bg.opacity2 = 1 / (self.timer > 45 and 10 / (55 - self.timer) or 1)

	self.award.scale = (self.timer < 15 and self.timer / 15 or 1)
	self.award.offsetY = game:scale(-100) * (self.timer < 25 and 0 or (self.timer < 35 and (self.timer - 25) / 10 or 1))
	if self.award.timer > 0 then
		self.award.angle = math.pi * 2 * (self.award.timer / 15)
		self.award.timer = self.award.timer - 1
	else
		self.award.angle = 0
	end

	self.complete.opacity = 255 * (self.timer < 25 and 0 or (self.timer < 35 and (self.timer - 25) / 10 or 1))

	self.yourscore.opacity = 255 * (self.timer < 30 and 0 or (self.timer < 40 and (self.timer - 30) / 10 or 1))
	self.yourscore.offsetY = 32 * (self.timer < 30 and 0 or (self.timer < 40 and (self.timer - 30) / 10 or 1))

	self.bestscore.opacity = 255 * (self.timer < 35 and 0 or (self.timer < 45 and (self.timer - 35) / 10 or 1))
	self.bestscore.offsetY = 32 * (self.timer < 35 and 0 or (self.timer < 45 and (self.timer - 35) / 10 or 1))

	if input:isKeyPressed("escape") then self:startTransition(2) end

	self.backButton:update()
	self.nextButton:update()

	if (self.timer == 55) then
		game:goBack(self.backCount)
	end

	if (self.timer < 45) or (self.timer > 45 and self.timer < 55) then
		self.timer = self.timer + 1
	end
end

function ScreenWin:draw()
	graphics.setFont(self.font)

	graphics.setColor(self.bg.r, self.bg.g, self.bg.b, self.bg.opacity * self.bg.opacity2)
	graphics.rectangle("fill", 0, 0, game.width, game.height)

	graphics.setColor(255, 255, 255, 255 * self.bg.opacity2)
	graphics.draw(self.award.sprite, self.award.x, (self.award.y + self.award.offsetY) * self.bg.opacity2, self.award.angle, self.award.scale, nil, self.award.oX, self.award.oY)

	graphics.setColor(self.text.r, self.text.g, self.text.b, self.complete.opacity * self.bg.opacity2)
	graphics.print(self.complete.text, self.complete.x, (self.complete.y) * self.bg.opacity2)

	graphics.setColor(self.text.r, self.text.g, self.text.b, self.yourscore.opacity * self.bg.opacity2)
	graphics.print(self.yourscore.text, self.yourscore.x, (self.yourscore.y + self.yourscore.offsetY) * self.bg.opacity2)

	graphics.setColor(self.text.r, self.text.g, self.text.b, self.bestscore.opacity * self.bg.opacity2)
	graphics.print(self.bestscore.text, self.bestscore.x, (self.bestscore.y + self.bestscore.offsetY) * self.bg.opacity2)

	graphics.setColor(255, 255, 255, self.bg.opacity * self.bg.opacity2)
	self.backButton:draw()
	self.nextButton:draw()
end