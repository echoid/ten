require "components/resources"
require "components/vocabulary"
require "components/sprite-button"
require "components/sprite-toogle-button"
require "components/text-button"
require "components/screen-menu"
require "components/screen-about"
require "components/screen-campaign"
require "components/screen-game"
require "components/screen-win"

firstScreen = ScreenMenu