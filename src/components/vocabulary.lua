vocabulary = {
	campaign =		{en = "campaign",		ru = "кампания"},
	authors =		{en = "authors",		ru = "авторы"}, 
	designer =		{en = "designer:",		ru = "дизайнер:"},
	developer =		{en = "developer:",		ru = "разработчик:"},
	quick_game =	{en = "quick game",		ru = "быстрая игра"},
	complete =		{en = "complete!",		ru = "пройдено!"},
	your_score =	{en = "your score: ",	ru = "ваш счет: "},
	best_score =	{en = "best score: ",	ru = "лучший счёт: "}
}