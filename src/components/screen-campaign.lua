ScreenCampaign = Screen:new()

function ScreenCampaign:load()
	self.tilesize = game:scale(71)
	self.bordersize = game:scale(10)
	self.blocksize = self.tilesize + self.bordersize

	self.timer = 0
	self.slidetimer = -1
	self.slidedir = 0
	self.slidetimermax = 10
	self.slide = false
	self.swipe = false
	self.font = game.resources["system-small"]
	self.fontButton = game.resources["system-big"]

	self.page = 1
	self.pages = 18 / 6

	self.bg = {}
	self.bg.r, self.bg.g, self.bg.b = game:getColorPalette("bg")
	self.bg.opacity = 0
	self.bg.opacity2 = 1

	self.text1 = {}
	self.text1.r, self.text1.g, self.text1.b = game:getColorPalette("text")

	self.text2 = {}
	self.text2.r, self.text2.g, self.text2.b = game:getColorPalette("text2")

	self.indicator = {}
	self.indicator.x = game.width / 2
	self.indicator.y = game.height / 2 + self.blocksize + game:scale(38)
	self.indicator.opacity = 0

	self.labels = {}
	self.buttons = {}

	self.campaign = {}
	self.campaign.text = game:translate("campaign")
	self.campaign.x = game.width / 2 - self.font:getWidth(self.campaign.text) / 2
	self.campaign.y = game:scale(64)
	self.campaign.offsetY = 0
	self.campaign.maxOffsetY = 0
	self.campaign.opacity = 0
	self.campaign.timerOffset = 5
	table.insert(self.labels, self.campaign)

	self.level = game.resources["level"]

	self.blocks = {}
	self.blocks.opacity = 0
	self.blocks.offsetX = 0
	self.blocks.x = game.width / 2 - self.blocksize * 1.5
	self.blocks.y = game.height / 2 - self.blocksize
	self.blocks.width = self.blocksize * 3
	self.blocks.height = self.blocksize * 2
	for j = 1, 2 do
		for i = 1, 3 do
			local x = self.blocks.x + self.blocksize * 0.5 + (i - 1) * self.blocksize
			local y = self.blocks.y + self.blocksize * 0.5 + (j - 1) * self.blocksize
			table.insert(self.blocks, {x = x, y = y, scale = 1})
		end
	end
	self.blocks.callback = function(n)
		game:pushScreen(ScreenGame)
	end
	self.blocks.sound = game.resources["button"]

	local x = game:scale(20)
	local y = game:scale(20)
	local cb = function() self:startTransition() end
	self.backButton = SpriteButton:new("button-back", "left", x, y, cb)
	table.insert(self.buttons, self.backButton)

	local x = game.width / 2 - self.blocksize
	local y = game.height / 2 + self.blocksize + game:scale(16)
	local cb = function() self:prevPage("slide") end
	local snd = game.resources["list"]
	self.prevButton = SpriteButton:new("button-left", "left", x, y, cb, snd)
	table.insert(self.buttons, self.prevButton)

	local x = game.width / 2 + self.blocksize
	local y = game.height / 2 + self.blocksize + game:scale(16)
	local cb = function() self:nextPage("slide") end
	self.nextButton = SpriteButton:new("button-right", "right", x, y, cb, snd)
	table.insert(self.buttons, self.nextButton)

	self.prevSpriteEn = self.prevButton.sprite
	self.prevSpriteDs = game.resources["button-left-off"]

	self.nextSpriteEn = self.nextButton.sprite
	self.nextSpriteDs = game.resources["button-right-off"]

	self.soundEn = self.prevButton.sound
	self.soundDs = game.resources["list-off"]
end

function ScreenCampaign:startTransition()
	if self.timer == 45 then self.timer = 46 end
end

function ScreenCampaign:update()
	local tim = self.timer

	self.bg.opacity = 17 * (tim < 15 and tim or 15)
	self.bg.opacity2 = 1 / (tim > 45 and 10 / (55 - tim) or 1)

	self.indicator.y = self.indicator.y * self.bg.opacity2
	self.indicator.opacity = 150 * (tim < 5 and 0 or (tim < 10 and (tim - 5) / 5 or 1)) * self.bg.opacity2

	for _, label in pairs(self.labels) do
		local off = label.timerOffset
		local progress = (tim < off and 0 or (tim < 5 + off and (tim - off) / 5 or 1))
		label.opacity = progress * 255
		label.offsetY = progress * label.maxOffsetY
	end
	
	if input:isKeyPressed("escape") then self:startTransition() end
	
	self.prevButton.enable = (self.page > 1)
	self.nextButton.enable = (self.page < 3)
	self.prevButton.sprite = (self.prevButton.enable and self.prevSpriteEn or self.prevSpriteDs)
	self.nextButton.sprite = (self.nextButton.enable and self.nextSpriteEn or self.nextSpriteDs)
	self.prevButton.sound = (self.prevButton.enable and self.soundEn or self.soundDs)
	self.nextButton.sound = (self.nextButton.enable and self.soundEn or self.soundDs)
	self.prevButton.y = self.prevButton.y * self.bg.opacity2
	self.nextButton.y = self.prevButton.y
	for _, but in pairs(self.buttons) do
		but.active = not self.swipe
		but:update()
	end

	self.blocks.opacity = self.bg.opacity * self.bg.opacity2

	local mx = input:getMouseX()
	local my = input:getMouseY()

	if self.slide then
		self.blocks.offsetX = (self.slidetimermax - self.slidetimer) / self.slidetimermax * game.width * 1.2 * -self.slidedir
	end

	if not self.swipe and not self.slide and self.blocks.offsetX ~= 0 then
		self.blocks.offsetX = self.blocks.offsetX * 0.8
		if math.abs(self.blocks.offsetX) < 1 then
			self.blocks.offsetX = 0
		end
	end

	if self.swipe then
		self.blocks.offsetX = mx - self.mouseStartX
		local maxOX = game.width / 5
		if self.page == 1 and self.blocks.offsetX > maxOX then
			self.blocks.offsetX = maxOX
		end
		if self.page == self.pages and self.blocks.offsetX < -maxOX then
			self.blocks.offsetX = -maxOX
		end
	end

	if self.mouseStartX and not self.swipe and not self.slide then
		if math.pow(mx - self.mouseStartX, 2) + math.pow(my - self.mouseStartY, 2) > game:scale(10) then
			self.swipe = true
		end
	end

	if input:isMousePressed() and not self.swipe and my >= self.blocks.y and my < self.blocks.y + self.blocks.height and not self.slide then
		self.mouseStartX = mx
		self.mouseStartY = my
	end

	if input:isMouseReleased() and not self.slide then
		if self.swipe then
			if self.blocks.offsetX < -game.width / 5 then
				self:nextPage("swipe")
			elseif self.blocks.offsetX >  game.width / 5 then
				 self:prevPage("swipe")
			else
				audio.play(game.resources["list-off"])
			end
		end
		self.mouseStartX = nil
		self.mouseStartY = nil
		self.swipe = false
	end

	for i = 1, 6 do
		if self.swipe or self.slide then
			self.blocks[i].isOver = false
			self.blocks[i].scale = 1
		else
			local x = self.blocks[i].x - self.tilesize / 2
			local y = self.blocks[i].y - self.tilesize / 2
			self.blocks[i].scale = (input:isMouseDown() and self.blocks[i].isOver and 0.95 or (self.blocks[i].isOver and 1.05 or 1))
			if self.blocks[i].isOver and input:isMouseReleased() then
				self.blocks[i].isOver = false
				audio.play(self.blocks.sound)
				self.blocks.callback(i + 6 * (self.page - 1))
			end
			self.blocks[i].isOver = (mx >= x and my >= y and mx < x + self.tilesize and my < y + self.tilesize)
		end
	end

	if tim == 15 then self.timer, tim = 45, 45 end
	if tim == 55 then game:goBack() end
	if (tim < 45) or (tim > 45 and tim < 55) then self.timer = self.timer + 1 end
	if self.slidetimer == 0 then
		self.blocks.offsetX = game.width * 0.2 * -self.slidedir
		self.page = self.page + self.slidedir
		self.slidedir = 0
		self.slide = false
	end
	if self.slidetimer >= 0 then self.slidetimer = self.slidetimer - 1 end
end

function ScreenCampaign:prevPage(type)
	if self.page > 1 and not self.swipe and not self.slide and type == "slide" then
		self.slidetimer = self.slidetimermax
		self.slidedir = -1
		self.slide = true
		return
	end
	if self.page > 1 and type == "swipe" then
		audio.play(game.resources["list"])
		self.page = self.page - 1
		if self.blocks.offsetX > 0 then
			self.blocks.offsetX = self.blocks.offsetX - game.width
		end
	end
end

function ScreenCampaign:nextPage(type)
	if self.page < self.pages and not self.swipe and not self.slide and type == "slide" then
		self.slidetimer = self.slidetimermax
		self.slidedir = 1
		self.slide = true
		return
	end
	if self.page < self.pages and type == "swipe" then
		audio.play(game.resources["list"])
		self.page = self.page + 1
		if self.blocks.offsetX < 0 then
			self.blocks.offsetX = game.width + self.blocks.offsetX
		end
	end
end

function ScreenCampaign:drawLevels(offsetX, offsetPage)
	local s = 255 / self.bg.opacity
	local ox = self.tilesize / 2
	local oy = self.tilesize / 2
	local toy = self.fontButton:getHeight() / 2

	for i = 1, 6 do
		local x = self.blocks[i].x + self.blocks.offsetX + offsetX
		local y = self.blocks[i].y
		local s2 = s * self.blocks[i].scale
		graphics.setColor(255, 255, 255, self.blocks.opacity)
		graphics.draw(self.level, x, y, 0, s2, nil, ox, oy)

		local num = i + 6 * (self.page + offsetPage - 1)
		graphics.setColor(self.text2.r, self.text2.g, self.text2.b, self.bg.opacity * self.bg.opacity2)
		graphics.print(num, x, (y - game:scale(4)) * self.bg.opacity2, 0, s2, nil, self.fontButton:getWidth(num) / 2, toy)
	end
end

function ScreenCampaign:drawIndicator(pos, page)
	local x = self.indicator.x + pos * game:scale(16)
	local dr = game:scale(5) * (self.page == page and 1 or 0)
	if self.slide then
		dr = dr * (self.slidetimer / self.slidetimermax)
	elseif self.swipe then
		dr = dr * ((game.width - math.abs(self.blocks.offsetX)) / game.width)
	end
	local r = game:scale(3) + dr
	graphics.setColor(255, 255, 255, self.indicator.opacity)
	graphics.circle("fill", x, self.indicator.y, r)
end

function ScreenCampaign:draw()
	graphics.setFont(self.font)

	graphics.setColor(self.bg.r, self.bg.g, self.bg.b, self.bg.opacity * self.bg.opacity2)
	graphics.rectangle("fill", 0, 0, game.width, game.height)

	for _, label in pairs(self.labels) do
		graphics.setColor(self.text1.r, self.text1.g, self.text1.b, label.opacity * self.bg.opacity2)
		graphics.print(label.text, label.x, (label.y + label.offsetY) * self.bg.opacity2)
	end

	graphics.setFont(self.fontButton)

	self:drawLevels(0, 0)

	if self.slidetimer >= 0 then self:drawLevels(game.width * self.slidedir, self.slidedir) end
	if self.blocks.offsetX < 0 then
		if self.page < self.pages then
			self:drawLevels( game.width,  1)
		end
	end

	if self.blocks.offsetX > 0 then
		if self.page > 1 then
			self:drawLevels(-game.width, -1)
		end
	end

	graphics.setColor(255, 255, 255, self.bg.opacity * self.bg.opacity2)
	for _, but in pairs(self.buttons) do but:draw() end

	for i = 1, self.pages do
		if self.pages % 2 == 1 then
			self:drawIndicator(i - self.pages / 2 - 0.5, i)
		else
			self:drawIndicator(i - self.pages + 0.5, i)
		end
	end
end