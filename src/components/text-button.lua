TextButton = {}

function TextButton:new(text, x, y, callback)
	self.__index = self

	local button = {}
	button.isOver = false
	button.rx = x
	button.ry = y
	button.scale = 1
	TextButton.setText(button, text)
	button.callback = callback
	button.sound = game.resources["button"]
	return setmetatable(button, self)
end

function TextButton:setText(text)
	self.text = text
	self.width = game.resources["system-big"]:getWidth(self.text)
	self.height = game.resources["system-big"]:getHeight()
	self.oX = self.width / 2
	self.oY = self.height / 2
	self.x = self.rx
	self.y = self.ry - game.resources["system-big"]:getHeight() / 3
end

function TextButton:update()
	self.isOver = input:isMouseIn({self.x - self.oX, self.y - self.oY, self.width, self.height})
	if input:isMouseReleased() and self.isOver then
		self.isOver = false
		audio.play(self.sound)
		self.callback()
	end
	self.scale = (self.isOver and (input:isMouseDown() and 0.95 or 1.05) or 1)
end

function TextButton:draw()
	graphics.setColor(game:getColorPalette("text"))
	graphics.setFont(game.resources["system-big"])
	graphics.print(self.text, self.x, self.y, 0, self.scale, nil, self.oX, self.oY)
end