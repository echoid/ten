ScreenGame = Screen:new()

function ScreenGame:init()
	self.resetTimer = 0

---------------------01------02------03------04------05------06------07------08------09------10--
	local sizes = {{1, 2}, {2, 2}, {1, 2}, {1, 2}, {2, 1}, {1, 2}, {1, 1}, {1, 1}, {1, 1}, {1, 1}}
	local posit = {{1, 1}, {2, 1}, {4, 1}, {1, 3}, {2, 3}, {4, 3}, {2, 4}, {3, 4}, {1, 5}, {4, 5}}

	if self.blocks then
		self.blocks_old = {}
		for i = 1, 10 do
			self.blocks_old[i] = {}
			self.blocks_old[i].sprite = self.blocks[i].sprite
			self.blocks_old[i].x = self.blocks[i].x
			self.blocks_old[i].y = self.blocks[i].y
		end
		self.resetTimer = 10
	end

	self.blocks = {}
	for i = 1, 10 do
		self.blocks[i] = {
			sprite = game.resources["block-" .. i],
			x = posit[i][1], y = posit[i][2], width = sizes[i][1], height = sizes[i][2],
			offsetX = 0, offsetY = 0
		}
	end

	self.score.val = 0
end

function ScreenGame:load()
	self.timer = 0

	self.bg = {}
	self.bg.r, self.bg.g, self.bg.b = game:getColorPalette("bg")
	self.bg.opacity = 0
	self.bg.opacity2 = 1

	self.text = {}
	self.text.r, self.text.g, self.text.b = game:getColorPalette("text")

	self.fontScore = game.resources["score"]
	self.fontSystem = game.resources["system-big"]

	self.tilesize = game:scale(56)
	self.bordersize = game:scale(2)
	self.blocksize = self.tilesize + self.bordersize

	self.field = {}
	self.field.width = self.tilesize * 4 + self.bordersize * 3
	self.field.height = self.tilesize * 5 + self.bordersize * 4
	self.field.x = game.width / 2 - self.field.width / 2
	self.field.y = game.height - self.field.x - self.field.height - game:scale(32)
	self.field.offsetY = 0
	self.field.maxOffsetY = game:scale(32)
	self.field.sprite = game.resources["field"]
	self.field.opacity = 0

	self.score = {}
	self.score.val = 0
	self.score.x = game.width / 2 - self.fontScore:getWidth(self.score.val) / 2
	self.score.y = self.field.y / 2 + game:scale(22) - self.fontScore:getHeight() / 2 - game:scale(48) 
	self.score.offsetY = 0
	self.score.maxOffsetY = game:scale(32)
	self.score.opacity = 0

	self.backButton = SpriteButton:new("button-back", "left", game:scale(20), game:scale(20), function() self:startTransition() end)
	self.restartButton = SpriteButton:new("button-restart", "right", game.width - game:scale(20), game:scale(20), function() self:init() end)

	self.soundBlock = game.resources["block"]

	self:init()
end

function ScreenGame:startTransition()
	if self.timer == 15 then self.timer = 25 end
end

function ScreenGame:checkHorizontal(b, map, direction)
	local addW = direction > 0 and b.width - 1 or 0
	local i = b.x + direction + addW
	while i > 0 and i < 5 do
		local isLast = false
		for j = b.y, b.y + b.height - 1 do
			if map[i][j] == 1 then
				isLast = true
				break
			end
		end
		if isLast then break end
			i = i + direction
		end

	return ((i - direction - addW) - b.x) * self.blocksize
end

function ScreenGame:checkVertical(b, map, direction)
	local addH = direction > 0 and b.height - 1 or 0
	local j = b.y + direction + addH
	while j > 0 and j < 6 do
		local isLast = false
		for i = b.x, b.x + b.width - 1 do
			if map[i][j] == 1 then
				isLast = true
				break
			end
		end
		if isLast then break end
		j = j + direction
	end

	return ((j - direction - addH) - b.y) * self.blocksize
end

function ScreenGame:getMap()
	local map = {{}, {}, {}, {}}

	for i = 1, 10 do
		for x = self.blocks[i].x, self.blocks[i].x + self.blocks[i].width - 1 do
			for y = self.blocks[i].y, self.blocks[i].y + self.blocks[i].height - 1 do
				map[x][y] = 1
			end
		end
	end

	return map
end

function ScreenGame:onBlockMoved()
	self.score.val = self.score.val + math.random(1000)
	audio.play(self.soundBlock)
end

function ScreenGame:update()
	if self.resetTimer > 0 then
		self.resetTimer = self.resetTimer - 1
		return
	end

	if input:isKeyPressed("escape") then self:startTransition() end

	if input:isKeyPressed("p") then
		game:pushScreen(ScreenWin, {score = self.score.val})
	end

	if self.blocks[2].x == 2 and self.blocks[2].y == 4 then
		game:pushScreen(ScreenWin, {score = self.score.val})
	end

	if self.movedBlock then
		self.movedBlock.offsetX = self.movedBlock.offsetX + input:getMouseX() - self.oldMouseX
		self.movedBlock.offsetY = self.movedBlock.offsetY + input:getMouseY() - self.oldMouseY
		self.oldMouseX = input:getMouseX()
		self.oldMouseY = input:getMouseY()
	end

	if input:isMouseReleased() and self.movedBlock then
		self.movedBlock.offsetX = self:inScope(self.movedBlock.offsetX, self.minOffsetX, self.maxOffsetX)
		self.movedBlock.offsetY = self:inScope(self.movedBlock.offsetY, self.minOffsetY, self.maxOffsetY)

		if math.abs(self.movedBlock.offsetX) > math.abs(self.movedBlock.offsetY) then
			if self.movedBlock.offsetX > self.tilesize / 2 then
				self.movedBlock.x = self.movedBlock.x + math.floor(self.movedBlock.offsetX / self.tilesize + 0.5)
				self:onBlockMoved()
			end

			if self.movedBlock.offsetX < -self.tilesize / 2 then
				self.movedBlock.x = self.movedBlock.x - math.floor(-self.movedBlock.offsetX / self.tilesize + 0.5)
				self:onBlockMoved()
			end
		else
			if self.movedBlock.offsetY > self.tilesize / 2 then
				self.movedBlock.y = self.movedBlock.y + math.floor(self.movedBlock.offsetY / self.tilesize + 0.5)
				self:onBlockMoved()
			end

			if self.movedBlock.offsetY < -self.tilesize / 2 then
				self.movedBlock.y = self.movedBlock.y - math.floor(-self.movedBlock.offsetY / self.tilesize + 0.5)
				self:onBlockMoved()
			end
		end

		self.movedBlock.offsetX = 0
		self.movedBlock.offsetY = 0
		self.movedBlock = nil
	end

	if input:isMousePressed() then
		local mX = input:getMouseX()
		local mY = input:getMouseY()
		for i = 1, 10 do
			local bX = self.field.x + (self.blocks[i].x - 1) * self.blocksize
			local bY = self.field.y + (self.blocks[i].y - 1) * self.blocksize + self.field.offsetY
			local bW = self.blocks[i].width * self.tilesize
			local bH = self.blocks[i].height * self.tilesize
			if (mX >= bX) and (mY >= bY) and (mX <= bX + bW) and (mY <= bY + bH) then			
				local map = self:getMap()
				self.movedBlock = self.blocks[i]

				self.oldMouseX = mX
				self.oldMouseY = mY

				self.minOffsetX = self:checkHorizontal(self.movedBlock, map, -1)
				self.maxOffsetX = self:checkHorizontal(self.movedBlock, map,  1)

				self.minOffsetY = self:checkVertical(self.movedBlock, map, -1)
				self.maxOffsetY = self:checkVertical(self.movedBlock, map,  1)

				break
			end
		end
	end

	self.bg.opacity = 17 * (self.timer < 15 and self.timer or 15)
	self.bg.opacity2 = 1 / (self.timer > 24 and 10 / (35 - self.timer) or 1)

	self.fontScore = game.resources["score" .. (self.score.val > 999 and self.score.val < 10000 and "-small" or "")]
	local progress = (self.timer < 5 and 0 or (self.timer < 15 and (self.timer - 5) / 10 or 1))
	self.score.x = game.width / 2 - self.fontScore:getWidth(self.score.val < 10000 and self.score.val or "∞") / 2
	self.score.y = self.field.y / 2 + game:scale(22) - self.fontScore:getHeight() / 2 - game:scale(48)
	self.score.opacity = progress * 255
	self.score.offsetY = progress * self.score.maxOffsetY

	local progress = (self.timer < 10 and 0 or (self.timer < 15 and (self.timer - 10) / 5 or 1))
	self.field.opacity = progress * 255
	self.field.offsetY = progress * self.field.maxOffsetY

	self.backButton:update()
	self.restartButton:update()

	if (self.timer == 35) then
		game:goBack()
	end

	if (self.timer < 15) or (self.timer > 24 and self.timer < 35) then
		self.timer = self.timer + 1
	end
end

function ScreenGame:inScope(n, min, max)
	if n < min then n = min end
	if n > max then n = max end
	return n
end

function ScreenGame:draw()
	if self.hide then return end
	
	graphics.setColor(self.bg.r, self.bg.g, self.bg.b, self.bg.opacity * self.bg.opacity2)
	graphics.rectangle("fill", 0, 0, game.width, game.height)

	local score = (self.score.val < 10000 and self.score.val or "∞")
	graphics.setFont(self.fontScore)
	graphics.setColor(self.text.r, self.text.g, self.text.b, self.score.opacity * self.bg.opacity2)
	graphics.print(score, self.score.x, self.score.y + self.score.offsetY * self.bg.opacity2)

	graphics.setColor(255, 255, 255, self.field.opacity * self.bg.opacity2)
	graphics.draw(self.field.sprite, self.field.x - game:scale(13), (self.field.y - game:scale(12) + self.field.offsetY)  * self.bg.opacity2)

	for i = 1, 10 do
		local b = self.blocks[i]

		local x = self.field.x + self.blocksize * (b.x - 1)
		local y = self.field.y + self.blocksize * (b.y - 1)
		if b == self.movedBlock then
			local offX = self:inScope(b.offsetX, self.minOffsetX, self.maxOffsetX)
			local offY = self:inScope(b.offsetY, self.minOffsetY, self.maxOffsetY)

			if math.abs(b.offsetX) > math.abs(b.offsetY) then
				x = x + offX
			else
				y = y + offY
			end
		end

		if self.resetTimer > 0 then
			sb = self.blocks_old[i]
			local sx = self.field.x + self.blocksize * (sb.x - 1)
			local sy = self.field.y + self.blocksize * (sb.y - 1)

			x = x + (sx - x) * (self.resetTimer / 10)
			y = y + (sy - y) * (self.resetTimer / 10)
		end

		graphics.draw(b.sprite, x, y + self.field.offsetY)
	end

	graphics.setColor(255, 255, 255, self.bg.opacity * self.bg.opacity2)
	self.backButton:draw()
	self.restartButton:draw()
end

function ScreenGame:resume()
	self:init()
end