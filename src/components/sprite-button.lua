SpriteButton = {}

function SpriteButton:new(sprite, align, x, y, callback, sound)
	self.__index = self

	local button = {}
	button.align = align
	button.isOver = false
	button.isHit = false
	if type(sprite) == "string" then
		button.sprite = game.resources[sprite]
	else
		button.sprite = sprite
	end
	button.width = button.sprite:getWidth()
	button.height = button.sprite:getHeight()
	button.oX = button.width / 2
	button.oY = button.height / 2
	button.x = x + button.oX * (button.align:sub(1, 1) == "r" and -1 or 1)
	button.y = y + button.oY * (button.align:sub(2, 2) == "b" and -1 or 1)
	button.scale = 1
	button.callback = callback
	button.enable = true
	button.active = true
	button.sound = sound or game.resources["button"]
	return setmetatable(button, self)
end

function SpriteButton:update()
	if input:isMouseReleased() and self.isHit then
		self.isHit = false
		self.isOver = false
		audio.play(self.sound)
		self.callback()
	end

	self.isOver = input:isMouseIn({self.x - self.oX, self.y - self.oY, self.width, self.height}) and self.active
	self.isHit = self.isOver and input:isMouseDown() and self.active

	self.scale = (self.isHit and 0.95 or (self.isOver and 1.05 or 1))
end

function SpriteButton:draw()
	graphics.draw(self.sprite, self.x, self.y, 0, self.scale, nil, self.oX, self.oY)
end