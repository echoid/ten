require "base/base"
require "components/components"

function love.load()
	game:load("Ten", firstScreen, true)
end

function love.update(dt)
	game:update(dt)
end

function love.draw()
	game:draw()
end

function love.keypressed(key)
	input:press(key)
end

function love.keyreleased(key)
	input:release(key)
end

function love.mousepressed(key)
	input:press("mouse")
end

function love.mousereleased(key)
	input:release("mouse")
end