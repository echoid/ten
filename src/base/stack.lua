Stack = {
	data = {},
	length = 0
}

function Stack:new()
	self.__index = self
	return setmetatable({}, self)
end

function Stack:get(i)
	return self.data[i]
end

function Stack:last()
	return self.data[self.length]
end

function Stack:append(val)
	self.length = self.length + 1
	self.data[self.length] = val
end

function Stack:deleteLast()
	self.data[self.length] = nil
	self.length = self.length - 1
end

function Stack:clear()
	for i = 1, self.length do
		self.data[i] = nil
	end

	self.length = 0
end

function Stack:isEmpty()
	return self.length == 0 and true or false
end