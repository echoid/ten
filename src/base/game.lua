game = {}

function game:load(title, screen, showFPS)
	self.version = "0.2"

	self.title = title or "Game"
	self.showFPS = showFPS
	self.palette = {}

	self.langs = {"en", "ru"}
	self.lang = 1

	self.assets = {}
	self.assets[1.0] = "mdpi"
	self.assets[1.5] = "hdpi"
	self.assets[2.0] = "xhdpi"
	self.assets[3.0] = "xxhdpi"
	self.assets[4.0] = "xxxhdpi"
	
	window.setTitle(self.title)
	window.setMode(320, 480, {})

	self.width = window.getWidth()
	self.height = window.getHeight()

	if system.getOS() == "Android" then
		if self.width < 480 then
			self.nscale = 1
		elseif self.width <	640 then
			self.nscale = 1.5
		elseif self.width < 960 then
			self.nscale = 2
		elseif  self.width < 1080 then
			self.nscale = 3
		else
			self.nscale = 4
		end
	else
		self.nscale = 1
	end

	self.resources = {}
	for _, res in pairs(resources) do
		if res.type == "font" then
			self.resources[res.label] = graphics.newFont("resources/" .. res.path, self:scale(res.size))
		elseif res.type == "img" then
			self.resources[res.label] = graphics.newImage("resources/" .. self.assets[self.nscale] .. "/" .. res.path)
		elseif res.type == "sound" then
			self.resources[res.label] = audio.newSource("resources/sounds/" .. res.path, "static")
		end
	end

	self.screens = Stack:new()
	self:setScreen(screen)
end

function game:nextLang()
	self.lang = self.lang + 1
	if self.lang > #self.langs then
		self.lang = self.lang - #self.langs
	end
end

function game:prevLang()
	self.lang = self.lang - 1
	if self.lang < 1 then
		self.lang = self.lang + #self.langs
	end
end

function game:translate(word)
	return vocabulary[word][self.langs[self.lang]]
end

function game:loadPalette()
	local palette = self.resources["palette"]:getData()
	self:setColorPalette("bg", palette:getPixel(0, 0))
	self:setColorPalette("bg2", palette:getPixel(0, 1))
	self:setColorPalette("text", palette:getPixel(0, 2))
	self:setColorPalette("text2", palette:getPixel(0, 3))
	graphics.setBackgroundColor(self:getColorPalette("bg"))
end

function game:setColorPalette(name, r, g, b, a)
	self.palette[name] = {r = r, g = g, b = b, a = a}
end

function game:getColorPalette(name)
	return self.palette[name].r, self.palette[name].g, self.palette[name].b, self.palette[name].a
end

function game:scale(n)
	return n * self.nscale
end

function game:setScreen(screen, param)
	for i = 1, self.screens.length do
		self.screens:get(i):dispose()
	end
	self.screens:clear()

	self.screens:append(screen:new())
	self.screens:last():load(param)
end

function game:pushScreen(screen, param)
	if not self.screens:isEmpty() then
		self.screens:last():pause()
		self.screens:last().isPause = true
	end

	self.screens:append(screen:new())
	self.screens:last():load(param)
end

function game:goBack(n)
	n = n or 1
	while n > 0 do
		self.screens:last():dispose()
		self.screens:deleteLast()
		self.screens:last():resume()
		self.screens:last().isPause = nil
		n = n - 1
	end
end

function game:update(dt)
	self.screens:last():update(dt)
	input:update()

	local title = self.title .. (self.showFPS and (" [FPS = " .. timer.getFPS() .. "]") or "")
	window.setTitle(title)
end

function game:draw()
	for i = 1, self.screens.length do
		graphics.setColor(255, 255, 255)
		self.screens:get(i):draw()
	end
end

function game:exit()
	for i = 1, self.screens.length do
		self.screens:get(i):dispose()
	end
	event.quit()
end