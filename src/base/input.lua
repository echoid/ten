input = {
	keys = {},
	oldkeys = {}
}

function input:isMouseDown()
	return self.keys["mouse"]
end

function input:isMouseUp()
	return not self.keys["mouse"]
end

function input:isMousePressed(key)
	return self.keys["mouse"] and not self.oldkeys["mouse"]
end

function input:isMouseReleased(key)
	return not self.keys["mouse"] and self.oldkeys["mouse"]
end

function input:getMouseX()
	return mouse.getX()
end

function input:getMouseY()
	return mouse.getY()
end

function input:isMouseIn(r)
	local x, y = mouse.getX(), mouse.getY()
	if x >= r[1] and x < r[1] + r[3] and y >= r[2] and y < r[2] + r[4] then
		return true
	end
	return false
end


function input:isKeyDown(key)
	return self.keys[key]
end

function input:isKeyUp(key)
	return not self:isKeyDown(key)
end

function input:isKeyPressed(key)
	return self.keys[key] and not self.oldkeys[key]
end

function input:isKeyReleased(key)
	return not self.keys[key] and self.oldkeys[key]
end

function input:press(key)
	self.keys[key] = true
end

function input:release(key)
	self.keys[key] = nil
end

function input:update()
	self.oldkeys = {}
	for key, value in pairs(self.keys) do
		self.oldkeys[key] = value
	end
end