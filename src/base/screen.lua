Screen = {}

function Screen:new()
	self.__index = self
	return setmetatable({name = "base"}, self)
end

function Screen:load() end
function Screen:update() end
function Screen:draw() end
function Screen:pause() end
function Screen:resume() end
function Screen:dispose() end
