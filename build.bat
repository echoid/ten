@echo off

set src=src
set res=bin-res
set out=bin

echo|set /p="Clean %out%... "
rmdir /S /Q %out% 2> nul
mkdir %out%
echo done.

if not exist %res% (
	echo Build resources...
	call build-res.bat
	echo done.
)

echo|set /p="Copy source code... "
xcopy /E %src% %out% > nul
echo done.

echo|set /p="Copy resources... "
mkdir %out%\resources
xcopy /E %res% %out%\resources > nul
echo done.

if "%1" == "run" (
	love %out%\
)

echo Done.