#!/bin/bash

src="src"
res="bin-res"
out="bin"

printf "Clean $out... "
rm -fdr $out
mkdir -p $out
printf "done.\n"

if [ ! -d $res ]; then
	printf "Build resources... "
	./build-res.sh
	printf "done.\n"
fi

printf "Copy source code... "
cp -r $src/* $out/
printf "done.\n"

printf "Copy resources... "
mkdir $out/temp/resources
cp -r $res/* $out/resources/
printf "done.\n"

if [[ $1 == "run" ]]; then
	love $out/
fi

printf "Done.\n"